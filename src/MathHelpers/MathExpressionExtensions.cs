﻿using Flee.PublicTypes;
using System;

namespace Task01_OneDimensionalOptimization
{
    public static class MathExpressionExtensions
    {
        public static Func<double, double> CreateExpressionForX(this string expression, string varName = "x")
        {
            ExpressionContext context = new ExpressionContext();
            context.Variables[varName] = 0.0d;
            IDynamicExpression e = context.CompileDynamic(expression);

            Func<double, double> expressionEvaluator = (double input) =>
            {
                context.Variables[varName] = input;
                var result = (double)e.Evaluate();
                return result;
            };

            return expressionEvaluator;
        }
        
        public static Func<double, double, double> CreateExpressionForX1X2(this string expression)
        {
            ExpressionContext context = new ExpressionContext();
            context.Variables["x1"] = 0.0d;
            context.Variables["x2"] = 0.0d;
            IDynamicExpression e = context.CompileDynamic(expression);

            Func<double, double, double> expressionEvaluator = (double x1, double x2) =>
            {
                context.Variables["x1"] = x1;
                context.Variables["x2"] = x2;
                var result = (double)e.Evaluate();
                return result;
            };

            return expressionEvaluator;
        }

        public static double Diff(this Func<double,double> function, double x)
        {
            const double dx = 1E-12;
            return (function(x + dx) - function(x)) / dx;
        }

    }
}
