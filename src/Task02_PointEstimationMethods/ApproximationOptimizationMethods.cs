﻿using System;
using Task01_OneDimensionalOptimization;

namespace Task02_PointEstimationMethods
{
    public static class ApproximationOptimizationMethods
    {
        public static double QuadraticOptimizationMethod(Func<double, double> function, double a, double b, double c, double epsilon)
        {
            int i = 0;

            double xOpt = (a + b) / 2;

            var x1 = a;
            var x2 = xOpt;
            var x3 = b;

            while (Math.Abs(x3 - x1) > epsilon)
            {
                i++;
                var f1 = function(x1);
                var f2 = function(x2);
                var f3 = function(x3);

                var a0 = f1;
                var a1 = (f2 - f1) / (x2 - x1);
                var a2 = 1 / (x3 - x2) * ((f3 - f1) / (x3 - x1) - (f2 - f1) / (x2 - x1));

                xOpt = (x2 + x1) / 2 - a1 / (2 * a2);

                if (xOpt < x2)
                {
                    x1 = x1;
                    x3 = x2;
                    x2 = xOpt;
                }
                else
                {
                    x1 = x2;
                    x3 = x3;
                    x2 = xOpt;
                }
            }
            Console.WriteLine($"Iterations: {i}");

            return xOpt;

        }

        public static double CubiqOptimizationMethod(Func<double, double> function, double x1, double x2, double c, double epsilon)
        {
            double xOpt = (x2 + x1) / 2;

            //var a0 = function(a);
            //var a1 = (function(b) - a0) / (b-a);
            //var a2 = (function.Diff(a) - a1) / (a-b);
            //var a3 = (function.Diff(b) - a1 - a2*(b-a)) / Math.Pow(b-a, 2);

            var f1 = function(x1);
            var f2 = function(x2);
            var fd1 = function.Diff(x1);
            var fd2 = function.Diff(x2);

            var z = (3 * (f1 - f2) / (x2 - x1)) + fd1 + fd2;

            double w;
            if (x1 < x2)
            {
                w = Math.Sqrt(z * z - fd1 * fd2);
            }
            else
            {
                w = -Math.Sqrt(z * z - fd1 * fd2);
            }

            var μ = (fd2 + w - z) / (fd2 - fd1 + 2 * w);

            xOpt = μ < 0 ? x2 : μ >= 0 && μ <= 1 ? x2 - μ * (x2 - x1) : x1;

            return xOpt;
        }
    }
}
