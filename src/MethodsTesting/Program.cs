﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Task01_OneDimensionalOptimization;
using Task02_PointEstimationMethods;
using Task03_SteepestDescentMethod;
using Task04_NewtonOptimizationMethod;
using Task05_ZeroOrderOptimizationMethods;

namespace Task02_PointEstimationMethods
{
    class Program
    {
        static void Main()
        {
            const double epsilon = 1e-2;

            Console.WriteLine("Welcome to the One Dimensional Optimizer by Oleksii Hornostal");
            Console.WriteLine("Please enter the function (ex., 2 * x ^ 3 - 12 * x ^ 2 + 18 * x + 3):");
            var function = Console.ReadLine().CreateExpressionForX();
            Console.WriteLine("Please enter the interval in form like a,b");
            var interval = Console.ReadLine();
            var inputedA = int.Parse(interval.Split(',')[0]);
            var inputedB = int.Parse(interval.Split(',')[1]);

            Console.WriteLine($"Epsilon: {epsilon}");
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Calculating Minimum.");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Dichotomy Method.");
            double a = inputedA;
            double b = inputedB;
            var c = 1;
            var x = OneDimensionOptimizationMethods.DichotomyMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Golden Ratio Method.");
            a = inputedA;
            b = inputedB;
            c = 1;
            x = OneDimensionOptimizationMethods.GoldenRatioMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Fibonacci Method.");
            a = inputedA;
            b = inputedB;
            c = 1;
            x = OneDimensionOptimizationMethods.FibonacciMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Quadratic Optimization Method.");
            a = inputedA;
            b = inputedB;
            c = 1;
            x = ApproximationOptimizationMethods.QuadraticOptimizationMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Cubiq Optimization Method.");
            a = inputedA;
            b = inputedB;
            c = 1;
            x = ApproximationOptimizationMethods.CubiqOptimizationMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");


            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Calculating Maximum.");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Dichotomy Method.");
            a = inputedA;
            b = inputedB;
            c = -1;
            x = OneDimensionOptimizationMethods.DichotomyMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Golden Ratio Method.");
            a = inputedA;
            b = inputedB;
            c = -1;
            x = OneDimensionOptimizationMethods.GoldenRatioMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");

            Console.WriteLine("----------------------------");
            Console.WriteLine("Fibonacci Method.");
            a = inputedA;
            b = inputedB;
            c = -1;
            x = OneDimensionOptimizationMethods.FibonacciMethod(function, a, b, c, epsilon);
            Console.WriteLine($"F({x}) = {function(x)}");
            
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine("Multidimensional Optimization with Steepest Descent Method");
            Console.WriteLine("----------------------------------");
            var resultsSD = new List<Tuple<int, int, ulong, long, double, double>>();
            var rand = new Random(System.DateTime.UtcNow.Millisecond);
            
            for (int i = 0; i < 10; i++)
            {
                var timer = new Stopwatch();
                timer.Start();
                var x1 = rand.Next(1, 100);
                var x2 = rand.Next(1, 100);
                var res = GradientMethods.SteepestDescentOptimization(x1, x2, out ulong iterations);
                timer.Stop();
                var elapsed = timer.ElapsedMilliseconds;
                var funcRes = GradientMethods.RosenbrockFunction(res.Item1, res.Item2);
                
                Console.WriteLine($"Test #{i}. x1 = {x1}. x2 = {x2}. Iterations: {iterations}. Elapsed: {elapsed / 1000d}s");
                Console.WriteLine($"Optimal Values: x1 = {res.Item1} x2 = {res.Item2}. Minimum: {funcRes}");
                Console.WriteLine("----------------------------------");

                resultsSD.Add(new Tuple<int, int, ulong, long, double, double>(x1,x2,iterations,elapsed,res.Item1,res.Item2));
            }

            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine("Multidimensional Optimization with Newton Method");
            Console.WriteLine("----------------------------------");
            var resultsNM = new List<Tuple<int, int, ulong, long, double, double>>();

            for (int i = 0; i < resultsSD.Count; i++)
            {
                var timer = new Stopwatch();
                timer.Start();
                var x1 = resultsSD[i].Item1;
                var x2 = resultsSD[i].Item2;
                var res = NewtonOptimizationMethod.Optimize(x1, x2, out ulong iterations);
                timer.Stop();
                var elapsed = timer.ElapsedMilliseconds;
                var funcRes = GradientMethods.RosenbrockFunction(res.Item1, res.Item2);

                Console.WriteLine($"Test #{i}. x1 = {x1}. x2 = {x2}. Iterations: {iterations}. Elapsed: {elapsed / 1000d}s");
                Console.WriteLine($"Optimal Values: x1 = {res.Item1} x2 = {res.Item2}. Minimum: {funcRes}");
                Console.WriteLine("----------------------------------");

                resultsNM.Add(new Tuple<int, int, ulong, long, double, double>(x1, x2, iterations, elapsed, res.Item1, res.Item2));
            } 
            
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine("Multidimensional Optimization with Hooke-Jeeves (Pattern Search) Optimization Method");
            Console.WriteLine("----------------------------------");
            var resultsHJPS = new List<Tuple<int, int, ulong, long, double, double>>();

            for (int i = 0; i < resultsSD.Count; i++)
            {
                var timer = new Stopwatch();
                timer.Start();
                var x1 = resultsSD[i].Item1;
                var x2 = resultsSD[i].Item2;
                var res = HookeJeevesPatternSearchOptimization.Optimize(x1, x2, out ulong iterations);
                timer.Stop();
                var elapsed = timer.ElapsedMilliseconds;
                var funcRes = GradientMethods.RosenbrockFunction(res.Item1, res.Item2);

                Console.WriteLine($"Test #{i}. x1 = {x1}. x2 = {x2}. Iterations: {iterations}. Elapsed: {elapsed / 1000d}s");
                Console.WriteLine($"Optimal Values: x1 = {res.Item1} x2 = {res.Item2}. Minimum: {funcRes}");
                Console.WriteLine("----------------------------------");

                resultsHJPS.Add(new Tuple<int, int, ulong, long, double, double>(x1, x2, iterations, elapsed, res.Item1, res.Item2));
            }
            
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine("Multidimensional Optimization with Nelder-Mead Simplex Optimization Method");
            Console.WriteLine("----------------------------------");
            var resultsNMS = new List<Tuple<int, int, ulong, long, double, double>>();

            for (int i = 0; i < resultsSD.Count; i++)
            {
                var timer = new Stopwatch();
                timer.Start();
                var x1 = resultsSD[i].Item1;
                var x2 = resultsSD[i].Item2;
                var res = NelderMeadSimplexMethod.Optimize(x1, x2, out ulong iterations);
                timer.Stop();
                var elapsed = timer.ElapsedMilliseconds;
                var funcRes = GradientMethods.RosenbrockFunction(res.Item1, res.Item2);

                Console.WriteLine($"Test #{i}. x1 = {x1}. x2 = {x2}. Iterations: {iterations}. Elapsed: {elapsed / 1000d}s");
                Console.WriteLine($"Optimal Values: x1 = {res.Item1} x2 = {res.Item2}. Minimum: {funcRes}");
                Console.WriteLine("----------------------------------");

                resultsNMS.Add(new Tuple<int, int, ulong, long, double, double>(x1, x2, iterations, elapsed, res.Item1, res.Item2));
            }

            Console.WriteLine("Press any key to close.");
            Console.ReadKey();
        }
    }
}
