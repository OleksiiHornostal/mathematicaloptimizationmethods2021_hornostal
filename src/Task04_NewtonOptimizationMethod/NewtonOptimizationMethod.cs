﻿using System;

namespace Task04_NewtonOptimizationMethod
{
    public static class NewtonOptimizationMethod
    { 
        public static Tuple<double, double> Optimize(double x1, double x2, out ulong iterations)
        {
            double epsilon = 10e-7;
            double x1Step = double.MaxValue;
            double x2Step = double.MaxValue;
            iterations = 0;
            while(Math.Abs(x1Step) > epsilon || Math.Abs(x2Step) > epsilon)
            {
                iterations++;

                var inversedMatrix = InverseMatrix2x2(RosenbrockJacobian(x1,x2));
                var gradient = new[] { RozenbroсkGradientX1 (x1,x2), RozenbroсkGradientX2(x1,x2)};

                x1Step = -1 * (inversedMatrix[0, 0] * gradient[0] + inversedMatrix[0, 1] * gradient[1]);
                x2Step = -1 * (inversedMatrix[1, 0] * gradient[0] + inversedMatrix[1, 1] * gradient[1]);

                Console.WriteLine("Step: " + x1Step + "; " + x2Step);

                x1 += x1Step;
                x2 += x2Step;
            }

            return new Tuple<double,double>(x1,x2);
        }

        static double[,] InverseMatrix2x2(double[,] items)
        {
            double det = items[0,0] * items[1,1] - items[0,1] * items[1,0];
            if (det == 0.0) return null;
            return new double[,]
            {
                {  items[1,1] / det, -items[0,1] / det },
                { -items[1,0] / det,  items[0,0] / det } 
            };
        }

        static double RozenbroсkGradientX1(double x1, double x2)
        {
            return 200 * (x1 - x2 * x2);
        }
        static double RozenbroсkGradientX2(double x1, double x2)
        {
            return -800 * (x1 - x2 * x2) * x2 - 2 * (1 - x2);
        }

        static double RosenbrockFunc(double x1, double x2)
        {
            return (1 - x1) * (1 - x1) + 100 * (x2 - x1 * x1) * (x2 - x1 * x1);
        }

        static double[,] RosenbrockJacobian(double x1, double x2)
        {
            double[,] jacobian = new double[2, 2];
            jacobian[0, 0] = 200;
            jacobian[0, 1] = -400 * x2;
            jacobian[1, 0] = -800 * x2;
            jacobian[1, 1] = -800 * (x1 - 3 * x2 * x2) + 2;
            return jacobian;
        }
    }
}

