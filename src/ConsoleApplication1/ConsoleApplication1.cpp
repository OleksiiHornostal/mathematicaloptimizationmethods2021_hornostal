#include <iostream>
#include "math.h"
#define N 3
#define beta 0.5
#define alfa 1
#define gamma 2.9
#define T 1

using namespace std;

double f(double p[N])
{
    return 10 * pow(p[0] + 8, 2) + 9 * pow(p[1] - 2, 2) + 6 * pow(p[2] + 5, 2);
}
void main()
{
    int   s, j, k, L, H, m;
    double F[N + 1], C[N], FR, fc, eps, z, S[N], SumP, Fsum, P[N + 1][N], R[N], E[N];
    m = 0;
    for (j = 0; j < N + 1; j++)//Инициализация вершин
    {
        for (k = 0; k < N; k++)
        {
            if (j == 0)
            {
                P[j][k] = 0;
            }
            else
            {
                if (j == k + 1)
                {
                    P[j][k] = T / (N * sqrt(2)) * (sqrt(N + 1) - 1 + N);
                }
                else
                {
                    P[j][k] = T / (N * sqrt(2)) * (sqrt(N + 1) - 1);
                }
            }
        }
    }
    cout << "vvedite tochnost e>0" << endl;//ввод точности решения
    cin >> eps;
    while (1)
    {
        for (j = 0; j < N + 1; j++)//Расчет значений функции в вершина многогранника
        {
            F[j] = f(P[j]);
        }
        for (j = 0; j < N + 1; j++)
        {
            for (k = 0; k < N; k++)
            {
                cout << P[j][k] << "  ";
            }
            cout << "F[" << j << "]=" << F[j] << endl;
        }
        //cin.get();
        for (H = 0, L = 0, j = 0; j < N + 1; j++)//поиск H и L - индексов вершин с макс. и мин. значениями функции
        {
            if (F[j] > F[H])
            {
                H = j;
            }
            if (F[j] < F[L])
            {
                L = j;
            }
        }
        cout << "H=" << H << endl << "L=" << L << endl;
        for (k = 0; k <= N - 1; k++)//Поиск С - ценр тяжести всех вершин за исключением Pн
        {
            SumP = 0;
            for (j = 0; j < N + 1; j++)
            {
                SumP = SumP + P[j][k];
            }
            C[k] = (1.0 / N) * (SumP - P[H][k]);
            cout << "C[K]=" << C[k] << endl;
        }
        fc = f(C);
        cout << "fc=" << fc << endl;
        for (Fsum = 0, j = 0; j < N + 1; j++)//Проверка на завершение
        {
            Fsum = Fsum + pow(F[j] - fc, 2);
        }
        z = sqrt((1.0 / (N + 1)) * Fsum);
        cout << "z=" << z << endl;
        cout << endl << endl;
        if (z <= eps)
        {
            break;
        }
        else
        {
            for (k = 0; k <= N - 1; k++)//Отражение вершины
            {
                R[k] = C[k] + alfa * (C[k] - P[H][k]);
            }
            FR = f(R);
            if (FR < F[L])//Проверка отражения
            {//если ДА
                cout << "1+" << endl;
                for (k = 0; k <= N - 1; k++)//Отражение оказалось эффективным, приступаем к растяжению
                {
                    E[k] = C[k] + gamma * (R[k] - C[k]);
                }
                if (f(E) < f(R))//Проверка растяжения
                {
                    cout << "2+" << endl;
                    for (k = 0; k <= N - 1; k++)
                    {
                        P[H][k] = E[k];
                    }
                }
                else
                {
                    cout << "2-" << endl;
                    for (k = 0; k <= N - 1; k++)
                    {
                        P[H][k] = R[k];
                    }
                }
            }
            else// если НЕТ
            {
                cout << "1-" << endl;
                s = 1;
                for (j = 0; j < N + 1; j++)//Проверка
                {
                    if (j != H)
                    {
                        if (FR < F[j])
                        {
                            s = 0;
                            break;
                        }
                    }
                }
                if (s)
                {//Отражение оказалось неэффективным, приступаем к сжатию
                    cout << "3+" << endl;
                    if (FR < F[H])
                    {
                        cout << "4+" << endl;
                        for (k = 0; k <= N - 1; k++)//Сжатие будет идти в направлении от хН. Замещение вершины.
                        {
                            P[H][k] = R[k];
                        }
                    }
                    else
                    {
                        cout << "4-" << endl;
                    }
                    for (k = 0; k <= N - 1; k++)//Сжатие
                    {
                        S[k] = C[k] + beta * (P[H][k] - C[k]);
                    }
                    if (f(S) < F[H])//Проверка
                    {
                        cout << "5+" << endl;
                        for (k = 0; k <= N - 1; k++)//Сжатие оказалось эффективным, использем его. Замещение вершины
                        {
                            P[H][k] = S[k];
                        }
                    }
                    else
                    {//Сжатие оказалось неэффективным, приступаем к редукции. Перерасчет вершин.
                        cout << "5-" << endl;
                        for (j = 0; j < N + 1; j++)
                        {
                            for (k = 0; k <= N - 1; k++)
                            {
                                if (j != L)
                                {
                                    P[j][k] = P[L][k] + 0.5 * (P[j][k] - P[L][k]);
                                }
                            }
                        }
                    }
                }
                else
                {
                    cout << "3-" << endl;
                    for (k = 0; k <= N - 1; k++)
                    {
                        P[H][k] = R[k];
                    }
                }
            }
        }
        m = m + 1;
    }
    cout << "m=" << m << endl;
    cout << "x= " << C[0] << "  y= " << C[1] << "  z= " << C[2] << endl;
    cin.get(); cin.get();
}