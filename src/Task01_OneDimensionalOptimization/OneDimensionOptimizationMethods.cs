﻿using System;
using System.Collections.Generic;

namespace Task01_OneDimensionalOptimization
{
    public static class OneDimensionOptimizationMethods
    {
        public static double DichotomyMethod(Func<double, double> function, double a, double b, double c, double epsilon)
        {
            while (Math.Abs(b - a) > epsilon)
            {
                var x1 = (a + b) / 2;
                var F1 = function(x1 - epsilon);
                var F2 = function(x1 + epsilon);
                if (c * F1 >= c * F2)
                {
                    a = x1;
                }
                else
                {
                    b = x1;
                }
            }
            return (a + b) / 2;
        }

        public static double GoldenRatioMethod(Func<double, double> function, double a, double b, double c, double epsilon)
        {
            var Phi1 = (1 + Math.Sqrt(5)) / 2;

            while (Math.Abs(b - a) >= epsilon)
            {
                var x1 = b - (b - a) / Phi1;
                var x2 = a + (b - a) / Phi1;
                var F1 = function(x1 - epsilon);
                var F2 = function(x2 + epsilon);

                if (c * F1 >= c * F2)
                {
                    a = x1;
                }
                else
                {
                    b = x2;
                }
            }

            var x = (a + b) / 2;

            return x;
        }

        public static double FibonacciMethod(Func<double, double> function, double a, double b, double c, double epsilon)
        {
            var n = 1;
            while (FibonacciRecursion(n) < ((b - a) / (2 * epsilon)))
                n++;
            Console.WriteLine($"Calculated N: {n}");

            double x1 = 1, x2 = 1;
            while (n != 1)
            {
                double Fn_2 = FibonacciRecursion(n - 2);
                double Fn_1 = FibonacciRecursion(n - 1);
                double Fn = FibonacciRecursion(n);
                x1 = a + (b - a) * (Fn_2 / Fn);
                x2 = a + (b - a) * (Fn_1 / Fn);
                var F1 = function(x1 - epsilon);
                var F2 = function(x2 + epsilon);

                n = n - 1;

                if (c * F1 >= c * F2)
                {
                    a = x1;
                }
                else
                {
                    b = x2;
                }
            }

            var x = (x1 + x2) / 2;

            return x;
        }

        private static Dictionary<int, long> _cacheMemory = new Dictionary<int, long>();

        static long FibonacciRecursion(int n)
        {
            if (_cacheMemory.ContainsKey(n))
            {
                return _cacheMemory[n];
            }
            if (n < 2)
            {
                return n;
            }
            _cacheMemory[n] = FibonacciRecursion(n - 1) + FibonacciRecursion(n - 2);
            return _cacheMemory[n];
        }

        //private static double GetNthFibonacciNumber_Bine(int n)
        //{
        //    var sqrt5 = Math.Sqrt(5);

        //    var left = (1 + sqrt5) / 2;
        //    var right = (1 - sqrt5) / 2;

        //    return Math.Round((Math.Pow(left, n) - Math.Pow(right, n)) / sqrt5);
        //}
    }
}
