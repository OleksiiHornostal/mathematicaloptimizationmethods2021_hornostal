﻿using System;

namespace Task05_ZeroOrderOptimizationMethods
{
    public class HookeJeevesPatternSearchOptimization
    {
        static double RosenbrockFunc(double x1, double x2)
        {
            return (1 - x1) * (1 - x1) + 100 * (x2 - x1 * x1) * (x2 - x1 * x1);
        }

        public static Tuple<double, double> Optimize(double x1, double x2, out ulong iterations)
        {
            Func<double, double, double> func = RosenbrockFunc;

            double x1Old = double.MaxValue, x2Old = double.MaxValue;
            double[] delta = { 1, 1 };
            double alpha = 0.95;
            double espilon = 10e-10;

            iterations = 0;
            while (delta[0] > 0 && delta[1] > 0 && (delta[0] > espilon || delta[1] > espilon))
            {
                iterations++;

                if (x1 == x1Old && delta[0] > espilon)
                {
                    delta[0] *= alpha;
                }
                if (x2 == x2Old && delta[1] > espilon)
                {
                    delta[1] *= alpha;
                }

                var f0 = func(x1, x2);
                x1Old = x1;
                x2Old = x2;
                var x1new = x1 + delta[0];
                var x2new = x2;
                var fnew = func(x1new, x2new);

                if (fnew < f0)
                {
                    x1 = x1new;
                    x2 = x2new;
                }
                else
                {
                    x1new = x1 - delta[0];
                    x2new = x2;
                    fnew = func(x1new, x2new);
                    if (fnew < f0)
                    {
                        x1 = x1new;
                        x2 = x2new;
                    }
                }

                f0 = func(x1, x2);

                x1new = x1;
                x2new = x2 + delta[1];
                fnew = func(x1new, x1new);
                if (fnew < f0)
                {
                    x1 = x1new;
                    x2 = x2new;
                }
                else
                {
                    x1new = x1;
                    x2new = x2 - delta[1];
                    fnew = func(x1new, x2new);
                    if (fnew < f0)
                    {
                        x1 = x1new;
                        x2 = x2new;
                    }
                }

                var x1DoubleNext = x1 + x1 - x1Old;
                var x2DoubleNext = x2 + x2 - x2Old;
                if (func(x1DoubleNext, x2DoubleNext) < func(x1Old, x2Old))
                {
                    x1 = x1DoubleNext;
                    x2 = x2DoubleNext;
                }
            }

            return new Tuple<double, double>(x1, x2);
        }
    }
}
