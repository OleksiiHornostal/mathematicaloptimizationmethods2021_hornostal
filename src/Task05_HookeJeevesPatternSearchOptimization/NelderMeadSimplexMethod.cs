﻿using System;

namespace Task05_ZeroOrderOptimizationMethods
{
    public class NelderMeadSimplexMethod
    {
        static double RosenbrockFunc(double x1, double x2)
        {
            return (1 - x1) * (1 - x1) + 100 * (x2 - x1 * x1) * (x2 - x1 * x1);
        }

        public static Tuple<double, double> Optimize(double x1, double x2, out ulong iterations, bool fullOutput = false)
        {
            Func<double, double, double> f = RosenbrockFunc;

            int N = 2;
            double beta = 0.5;
            double alfa = 1;
            double gamma = 2.9;
            double T = 1;
            bool s;
            int j, k, L, H, m;
            double[] F = new double[N + 1];
            double[] C = new double[N];
            double FR, fc, eps = 10e-7, z;
            double[] S = new double[N];
            double SumP, Fsum;
            double[][] P = new double[N + 1][];
            double[] R = new double[N];
            double[] E = new double[N];
            m = 0;

            // Инициализация
            for (j = 0; j < N + 1; j++)
            {
                P[j] = new double[N];
                // Стартовые значения переменных
                if (j == 0)
                {
                    P[j][0] = x1;
                    P[j][1] = x2;
                }
                for (k = 0; k < N; k++)
                {
                    if (j != 0)
                    {
                        if (j == k + 1)
                        {
                            P[j][k] = T / (N * Math.Sqrt(2)) * (Math.Sqrt(N + 1) - 1 + N);
                        }
                        else
                        {
                            P[j][k] = T / (N * Math.Sqrt(2)) * (Math.Sqrt(N + 1) - 1);
                        }
                    }
                }
            }

            iterations = 0;
            while (true)
            {
                iterations++;

                for (j = 0; j < N + 1; j++)//Расчет значений функции Розенброка в вершина многогранника
                {
                    F[j] = f(P[j][0], P[j][1]);
                }
                for (j = 0; j < N + 1; j++)
                {
                    if (fullOutput)
                        for (k = 0; k < N; k++)
                        {
                            Console.WriteLine(P[j][k]);
                        }
                    if (fullOutput)
                        Console.WriteLine("F[" + j + "]=" + F[j]);
                }

                for (H = 0, L = 0, j = 0; j < N + 1; j++)//поиск H и L - индексов вершин с макс. и мин. значениями функции
                {
                    if (F[j] > F[H])
                    {
                        H = j;
                    }
                    if (F[j] < F[L])
                    {
                        L = j;
                    }
                }
                if (fullOutput)
                    Console.WriteLine("H=" + H + "\r\n" + "L=" + L);
                for (k = 0; k <= N - 1; k++)//Поиск С - ценр тяжести всех вершин за исключением Pн
                {
                    SumP = 0;
                    for (j = 0; j < N + 1; j++)
                    {
                        SumP = SumP + P[j][k];
                    }
                    C[k] = (1.0 / N) * (SumP - P[H][k]);
                    if (fullOutput)
                        Console.WriteLine("C[K]=" + C[k]);
                }
                fc = f(C[0], C[1]);
                if (fullOutput)
                    Console.WriteLine("fc=" + fc);
                for (Fsum = 0, j = 0; j < N + 1; j++)//Проверка на завершение
                {
                    Fsum = Fsum + Math.Pow(F[j] - fc, 2);
                }
                z = Math.Sqrt((1.0 / (N + 1)) * Fsum);
                if (fullOutput)
                {
                    Console.WriteLine("z=" + z);
                    Console.WriteLine();
                }
                if (z <= eps)
                {
                    break;
                }
                else
                {
                    for (k = 0; k <= N - 1; k++)//Отражение вершины
                    {
                        R[k] = C[k] + alfa * (C[k] - P[H][k]);
                    }
                    FR = f(R[0], R[1]);
                    if (FR < F[L])//Проверка отражения
                    {//если ДА
                        if (fullOutput)
                            Console.WriteLine("1+");
                        for (k = 0; k <= N - 1; k++)//Отражение оказалось эффективным, приступаем к растяжению
                        {
                            E[k] = C[k] + gamma * (R[k] - C[k]);
                        }
                        if (f(E[0], E[1]) < f(R[0], R[1]))//Проверка растяжения
                        {
                            if (fullOutput)
                                Console.WriteLine("2+");
                            for (k = 0; k <= N - 1; k++)
                            {
                                P[H][k] = E[k];
                            }
                        }
                        else
                        {
                            if (fullOutput)
                                Console.WriteLine("2-");
                            for (k = 0; k <= N - 1; k++)
                            {
                                P[H][k] = R[k];
                            }
                        }
                    }
                    else// если НЕТ
                    {
                        if (fullOutput)
                            Console.WriteLine("1-");
                        s = true;
                        for (j = 0; j < N + 1; j++)//Проверка
                        {
                            if (j != H)
                            {
                                if (FR < F[j])
                                {
                                    s = false;
                                    break;
                                }
                            }
                        }
                        if (s)
                        {//Отражение оказалось неэффективным, приступаем к сжатию
                            if (fullOutput)
                                Console.WriteLine("3+");
                            if (FR < F[H])
                            {
                                if (fullOutput)
                                    Console.WriteLine("4+");
                                for (k = 0; k <= N - 1; k++)//Сжатие будет идти в направлении от хН. Замещение вершины.
                                {
                                    P[H][k] = R[k];
                                }
                            }
                            else
                            {
                                if (fullOutput)
                                    Console.WriteLine("4-");
                            }
                            for (k = 0; k <= N - 1; k++)//Сжатие
                            {
                                S[k] = C[k] + beta * (P[H][k] - C[k]);
                            }
                            if (f(S[0], S[1]) < F[H])//Проверка
                            {
                                if (fullOutput)
                                    Console.WriteLine("5+");
                                for (k = 0; k <= N - 1; k++)//Сжатие оказалось эффективным, использем его. Замещение вершины
                                {
                                    P[H][k] = S[k];
                                }
                            }
                            else
                            {//Сжатие оказалось неэффективным, приступаем к редукции. Перерасчет вершин.
                                if (fullOutput)
                                    Console.WriteLine("5-");
                                for (j = 0; j < N + 1; j++)
                                {
                                    for (k = 0; k <= N - 1; k++)
                                    {
                                        if (j != L)
                                        {
                                            P[j][k] = P[L][k] + 0.5 * (P[j][k] - P[L][k]);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (fullOutput)
                                Console.WriteLine("3-");
                            for (k = 0; k <= N - 1; k++)
                            {
                                P[H][k] = R[k];
                            }
                        }
                    }
                }
                m = m + 1;
            }

            return new Tuple<double, double>(C[0], C[1]);
        }
    }
}
