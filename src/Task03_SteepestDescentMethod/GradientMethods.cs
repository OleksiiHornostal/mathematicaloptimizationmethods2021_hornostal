﻿using System;
using Task01_OneDimensionalOptimization;

namespace Task03_SteepestDescentMethod
{
    public static class GradientMethods
    {
        public static double RosenbrockFunction(double x1, double x2)
        {
            return (1 - x1) * (1 - x1) + 100 * (x2 - x1 * x1) * (x2 - x1 * x1);
        }

        //dx1 = -400 * x1 (x2-x1^2) - 2 (1-x1)
        private static double RosenbrockFunctionDiffX1(double x1, double x2)
        {
            return -400 * x1 * (x2 - x1 * x1) - 2 * (1 - x1);
        }

        //dx2 = 200 * x1 (x2-x1^2)
        private static double RosenbrockFunctionDiffX2(double x1, double x2)
        {
            return 200 * x1 * (x2 - x1 * x1);
        }

        public static Tuple<double, double> SteepestDescentOptimization(double x1, double x2, out ulong iterations)
        {
            const double epsilon = 1e-6;
            const int c = 1;

            var funcString = "(1-x1)^2+100*(x2-x1^2)^2";
            var func = funcString.CreateExpressionForX1X2();

            var gradX1 = RosenbrockFunctionDiffX1(x1, x2);
            var gradX2 = RosenbrockFunctionDiffX2(x1, x2);
            var optimizeByX1 = Math.Abs(gradX1) > Math.Abs(gradX2);

            iterations = 0;
            while (true)
            {
                iterations++;

                gradX1 = RosenbrockFunctionDiffX1(x1, x2);
                gradX2 = RosenbrockFunctionDiffX2(x1, x2);

                // Local Optimization
                if (optimizeByX1)
                {
                    var startX1 = x1;
                    var increment = gradX1 > 0 ? -1 : 1;
                    var point1 = func(x1, x2);
                    int inc = 1;
                    while (true)
                    {
                        x1 += increment * 0.01 * inc++;
                        var newPoint = func(x1, x2);
                        if (newPoint > point1) break;
                        point1 = newPoint;
                    }

                    var funcExpression = funcString.Replace("x2", x2.ToString()).CreateExpressionForX("x1");
                    x1 = OneDimensionOptimizationMethods.DichotomyMethod(funcExpression, x1 < startX1 ? x1 : startX1,
                        startX1 > x1 ? startX1 : x1, c, epsilon);

                    if (Math.Abs(startX1 - x1) < epsilon)
                        break;

                    optimizeByX1 = false;
                }
                else
                {
                    var startX2 = x2;
                    var increment = gradX2 > 0 ? -1 : 1;
                    var point1 = func(x1, x2);
                    int inc = 1;
                    while (true)
                    {
                        x2 += increment * 0.01 * inc++;
                        var newPoint = func(x1, x2);
                        if (newPoint > point1) break;
                        point1 = newPoint;
                    }

                    var funcExpression = funcString.Replace("x1", x1.ToString()).CreateExpressionForX("x2");
                    x2 = OneDimensionOptimizationMethods.DichotomyMethod(funcExpression, x2 < startX2 ? x2 : startX2,
                        startX2 > x2 ? startX2 : x2, c, epsilon);

                    if (Math.Abs(startX2 - x2) < epsilon)
                        break;

                    optimizeByX1 = true;
                }
            }

            return new Tuple<double, double>(x1, x2);
        }
    }
}